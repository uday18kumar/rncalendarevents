
import React, {Component} from 'react';
import { Platform,
         StyleSheet, 
         Text, 
         View,
         TouchableOpacity,
         AsyncStorage,
         TextInput,
         ScrollView,
         Animated,
         Dimensions
            } from 'react-native';

import Calendar from 'react-native-calendar';
import moment from 'moment';
import {responsiveFontSize,responsiveHeight,responsiveWidth} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modalbox';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

const HEADER_MAX_HEIGHT = responsiveHeight(58);
const HEADER_MIN_HEIGHT = responsiveHeight(30);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedDate: moment().format('L'),
          selectedMonth:null,
          months : ['January','February','March','April','May','June','July','August','September','Ocotber','November','December'],
          eventsData:[],
          isDateTimePickerVisible: false,
          eventTime:'Select Time',
          eventTimeUtc:'',
          eventLoc:'',
          eventName:'',
          eventDates:[],
          scrollY: new Animated.Value(0),
          viewHeight:responsiveHeight(55)
        };
        this.onDateChange = this.onDateChange.bind(this);
        this.onMonthChange = this.onMonthChange.bind(this);
      }
     
      

      componentDidMount = () => {
          let event_dates = [];
            AsyncStorage.getItem('Date_Events')
            .then((data)=>{
                if(data != null){
                    let event = JSON.parse(data);
                    this.setState({eventsData:event});
                   
                    for(let i=0; i < event.length ; i++)
                    {
                        event_dates[i] = moment(event[i].event_date).format('YYYY-MM-DD');
                    }
                    this.setState({eventDates:event_dates});
                }
            })
          
            // AsyncStorage.removeItem('Date_Events');
      };

    //   componentWillUpdate(prevProps,prevState){
    //       if(prevState.eventData != this.state.eventsData){
    //         AsyncStorage.getItem('Date_Events')
    //         .then((data)=>{
    //                 this.setState({eventData:data})
    //         })
    //       }
    //   }
      
    handleScroll(event) {
      this.setState({ viewHeight: this.state.viewHeight - event.nativeEvent.contentOffset.y})
     }

      render() {
        const headerHeight = this.state.scrollY.interpolate({
          inputRange: [0, HEADER_SCROLL_DISTANCE],
          outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
          extrapolate: 'clamp',
        });
        return (

              <View style={[styles.container]}>
           
                <ScrollView 
                  alwaysBounceVertical={false}
                   style={{ flex: 1, width: '100%' ,
                   backgroundColor:'transparent'
                  }}
                   contentContainerStyle={{ width: '100%' }}
                   onScroll={Animated.event(
                    [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                  )}
                   scrollEventThrottle={16}
                   removeClippedSubviews={false}
                   >
                   <View style={{marginTop:HEADER_MAX_HEIGHT}}>
                   <Text style={{fontSize:responsiveFontSize(1.6),
                                color:'grey',
                                paddingLeft:responsiveWidth(5),
                                marginTop:responsiveHeight(1)}}>
                                {moment(this.state.selectedDate).format('dddd')} ,{moment(this.state.selectedDate).format('LL')}</Text>
                {this.renderEvents()}
                   </View>
                
                </ScrollView>


                   {/* header starts */}
              <Animated.View 
              style={[styles.header,{height: headerHeight}]}
              >

              <View style={{backgroundColor:'#7C4DFF',
                        width:responsiveWidth(100),
                        alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={()=>this.addEvent()} 
                        style={{marginTop:responsiveHeight(5),
                        width:responsiveWidth(10)}}>
                    <Text style={{fontSize:responsiveFontSize(6),
                                color:'#fff',
                                textAlign:'right',
                                paddingRight:responsiveWidth(1),
                                fontWeight:'200'}}>+</Text>
                    </TouchableOpacity>
                </View>  
                
                {this.renderCalendar()}
                </Animated.View>  
            {/* header ends */}

            {/* modal for adding event     */}
            <Modal
              style={[styles.modal]}
              ref={"eventModal"}
              position={'center'}
              onClosed={()=>this.onModalClose()}
              backdropPressToClose={false}
              swipeToClose={false}
            >
                <View style={{
                                    flexDirection:'row',
                                    width:responsiveWidth(80),
                                    justifyContent:'center'}}>
                    <Text style={{fontSize:responsiveFontSize(2),
                                    width:responsiveWidth(70),
                                    marginTop:responsiveHeight(2),
                                    textAlign:'center'}}>Add Event on {moment(this.state.selectedDate).format('LL')}</Text>
                    
                    <TouchableOpacity style={{alignItems:'flex-end'}} 
                              onPress={()=>this.onModalClose()} >
                            <Text style={{alignItems:'flex-end',
                                            color:'#7C4DFF',
                                            fontSize:responsiveFontSize(2.5),
                                            marginTop:responsiveHeight(1)}}>X</Text> 
                    </TouchableOpacity>              
                </View>
                <View style={{flexDirection:'row',
                                width:responsiveWidth(75),
                                marginTop:responsiveHeight(5),}}>
                        <Text style={{fontSize:responsiveFontSize(2),width:responsiveWidth(20)}}>Event : </Text>
                        <TextInput 
                                    placeholder={'Event'}
                                    style={{width:responsiveWidth(50),
                                        borderBottomColor:'black',
                                        borderBottomWidth:1
                            
                                }}
                                    autoFocus={true}
                                    onChangeText={(text)=>this.onChangeEventName(text)}
                            />
                </View>
                
                <View style={{flexDirection:'row',
                            marginTop:responsiveHeight(2),
                            width:responsiveWidth(75)}}>
                        <Text style={{fontSize:responsiveFontSize(2),width:responsiveWidth(20)}}>Time : </Text>
                        <TouchableOpacity onPress={()=>this._showDateTimePicker()} style={{width:responsiveWidth(50)}}>
                                <Text style={{textAlign:'center'}}>{this.state.eventTime}</Text>
                        </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',marginTop:responsiveHeight(2),width:responsiveWidth(75)}}>
                        <Text style={{fontSize:responsiveFontSize(2),width:responsiveWidth(20)}}>Location : </Text>
                        <TextInput 
                                    placeholder={'Location'}
                                    style={{width:responsiveWidth(50),
                                            borderBottomColor:'black',
                                            borderBottomWidth:1
                                    }}
                                    onChangeText={(text)=>this.onChangeEventLoc(text)}
                            />
                </View>
                
                
                <TouchableOpacity style={{marginTop:responsiveHeight(2),
                                            width:responsiveWidth(30),
                                            height:responsiveWidth(8),
                                            backgroundColor:'#7C4DFF',
                                            justifyContent:'center',
                                            alignItems:'center',
                                            borderRadius: responsiveWidth(4)}}
                                onPress={()=>this.eventAdd()}
                                            >
                    <Text style={{fontSize:responsiveFontSize(2),color:'#fff'}}>
                        Add
                    </Text>
                </TouchableOpacity>
            </Modal>

            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
              mode={'time'}
            />
              </View>
  
        
        );
      }

    renderCalendar(){
      // alert('coming')
      return(
        <Calendar
        currentMonth={moment().currentMonth}       // Optional date to set the currently displayed month after initialization
        customStyle={calenderDesign} // Customize any pre-defined styles
        dayHeadings={['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']}               // Default: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
        eventDates={this.state.eventDates}       // Optional array of moment() parseable dates that will show an event indicator
        // events={[{date:'2015-07-01'}]}// Optional array of event objects with a date property and custom styles for the event indicator
        monthNames={this.state.months}                // Defaults to english names of months
              // Optional array of moment() parseable dates that will show an event indicator
        onDateSelect={(date) => this.onDateChange(date)} // Callback after date selection
        onSwipeNext={this.onSwipeNext}    // Callback for forward swipe event
        onSwipePrev={this.onSwipePrev}    // Callback for back swipe event      
        removeClippedSubviews={true}     // Set to false for us within Modals. Default: true
        scrollEnabled={true}              // False disables swiping. Default: False
        selectedDate={this.state.selectedStartDate}       // Day to be selected
        showControls={false}
        showEventIndicators={true}        // False hides event indicators. Default:False
        startDate={this.state.selectedDate}          // The first month that will display. Default: current month
        titleFormat1={'YYYY'}         // Format for displaying current month. Default: 'MMMM YYYY'
        titleFormat2={'MMMM'}         // Format for displaying current month. Default: 'MMMM YYYY'
        today={moment().format('L')}              // Defaults to today
        weekStart={0} // Day on which week starts 0 - Sunday, 1 - Monday, 2 - Tuesday, etc, Default: 1
    />

      )
    }


      compare(a,b) {
        if (a.event_time_utc < b.event_time_utc)
          return -1;
        if (a.event_time_utc > b.event_time_utc)
          return 1;
        return 0;
      }

    renderEvents(){
        let sortedArray = this.state.eventsData.sort(this.compare);

        return sortedArray.map((item,index)=>{
                if(moment(item.event_date).format('YYYY-MM-DD') == moment(this.state.selectedDate).format('YYYY-MM-DD'))
                    {
                      
                    return(   
                            <View style={{flexDirection:'row'}}>
                                <View style={{width:responsiveWidth(25),
                                            alignItems:'center',
                                            height:responsiveHeight(5),
                                            justifyContent:'center',
                                  }}>
                                    <Text style={{fontSize:responsiveFontSize(1.7)}}>{item.event_time}</Text>
                                </View>
                                <View style={{height:responsiveHeight(6)}}>
                                    <Text style={{fontSize:responsiveFontSize(3),
                                            color:getRandomColor()}}> . </Text>
                                </View>
                                <View style={{width:responsiveWidth(70),
                                            height:responsiveHeight(6),
                                            justifyContent:'center',}}>
                                <Text style={{fontSize:responsiveFontSize(2)}}>{item.event_name}</Text>
                                <Text style={{fontSize:responsiveFontSize(1.7),color:'grey'}}>{item.event_loc}</Text>
                                </View>
                            </View>
                    );
                }
            
        })
    }  


    onMonthChange(month){
        this.setState({
            selectedMonth: month 
        })
    }

    onDateChange(date) {
      let selDate = moment(date).format('L');
      this.setState({
        selectedDate: selDate,
      });
    }

    // Modal functions 
    onModalClose(){
        this.refs.eventModal.close();
    }
  
    addEvent(){
          this.refs.eventModal.open();
    }

    onChangeEventName(text){
        this.setState({
                eventName:text
        });
    }
    onChangeEventLoc(text){
        this.setState({
                eventLoc:text
        });
    }
      
    eventAdd(){
        let data = [];
        if(this.state.eventName == '' || this.state.eventTime == 'Select Time' || this.state.eventLoc == '')
        {
          alert('Please Enter All Fields')
          return true;
        }
        let eventAnddate = {
                            'event_date':this.state.selectedDate,
                            'event_name':this.state.eventName,
                            'event_loc':this.state.eventLoc,
                            'event_time':this.state.eventTime,
                            'event_time_utc':this.state.eventTimeUtc
                            };
        data.push(eventAnddate);

        AsyncStorage.getItem('Date_Events')
        .then((eventData)=>{
            if(eventData == null){
                let enDate = this.state.eventDates;
                enDate.push(moment(this.state.selectedDate).format('YYYY-MM-DD'));
                this.setState({eventsData:data,eventDates:enDate,eventTime:'Select Time'})
                AsyncStorage.setItem('Date_Events',JSON.stringify(data));
                // alert(moment(this.state.selectedDate).format('YYYY-MM-DD'));
            }
            else{
                let en = JSON.parse(eventData);
                en.push(eventAnddate);
                let enDate = this.state.eventDates;
                enDate.push(moment(this.state.selectedDate).format('YYYY-MM-DD'));
                this.setState({eventsData:en,eventDates:enDate,eventTime:'Select Time'});
                AsyncStorage.setItem('Date_Events',JSON.stringify(en));
                
            }
        }); 
        this.refs.eventModal.close();
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (time) => {
            
            let event_time = moment(time).format('hh:mm a');
            let event_time_utc = moment(time).format('x');
            this.setState({eventTime:event_time,eventTimeUtc:event_time_utc}); 
            
            this._hideDateTimePicker();
        }
      
}

 getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


const calenderDesign = {
    day: {fontSize: responsiveFontSize(2), textAlign: 'center',color:'#fff'},
    dayHeading:{color:'#fff'},
    calendarContainer:{backgroundColor:'#7C4DFF'},
    calendarControls:{width:responsiveWidth(35),flexDirection: 'column',height:responsiveHeight(10),padding: responsiveWidth(2)},
    calendarHeading:{borderWidth:0,borderTopWidth:0,borderBottomWidth: 0,color:'#fff'},
    dayButton:{borderBottomWidth:0,borderTopWidth:0,color:'#fff'},
    title:{fontSize:responsiveFontSize(3.5),color:'#fff',fontWeight:'300'},
    weekendDayButton:{backgroundColor:'#7C4DFF'},
    weekendHeading:{color:'#fff'},   
    selectedDayCircle:{backgroundColor:'#fff'},
    selectedDayText:{color:'#7C4DFF'},
    currentDayText:{color:'blue'},
    currentDayCircle:{backgroundColor:'#fff'},
    eventIndicator:{backgroundColor:'#fbc99b'}
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    zIndex:0
  },
  modal: {
    width:responsiveWidth(80),
    height:responsiveHeight(30),
    alignItems: 'center',
    zIndex:9999
  },
  header:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#7C4DFF',
    overflow:'hidden',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  }
 
});
